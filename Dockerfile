FROM prom/prometheus

ADD start-prometheus-with-env-vars.sh /
ADD prometheus.yml.tmpl /etc/prometheus/

ENTRYPOINT [ "/start-prometheus-with-env-vars.sh" ]
CMD [ "--config.file=/etc/prometheus/prometheus.yml", \
      "--storage.tsdb.path=/prometheus", \
      "--storage.tsdb.retention.time=5m" ]
