#! /bin/sh

export $(grep -v '^#' /run/secrets/$ENV_VARS_SECRET_NAME | xargs)

generate_prometheus_config() {
    rm -f /tmp/vinfra-discovery-services
    for account_id in $ACCOUNT_LIST
    do
        wget \
            -q \
            -O - \
            --header "Authorization: Bearer $VINFRA_TOKEN" \
            "https://api.vinfra.alessio.cc/v1/resources/$account_id/service" \
            | sed -e 's/^\[\(.*\)\]$/\1/' \
            | sed -e 's/, /\n/g' \
            | sed -e 's/^"\(.*\)"$/- \1/' >> /tmp/vinfra-discovery-services
        echo "" >> /tmp/vinfra-discovery-services
    done
    cp /etc/prometheus/prometheus.yml.tmpl /etc/prometheus/prometheus.yml
    sed -e 's/- /    - tasks./' /tmp/vinfra-discovery-services >> /etc/prometheus/prometheus.yml
}

reload_prometheus_config() {
    while true
    do
        sleep 60
        generate_prometheus_config
        kill -SIGHUP "$(pgrep -f /bin/prometheus)"
    done
}

env | while IFS='=' read -r n v
do
    if [ ! "${n%%PROMETHEUS*}" ]
    then
        sed -i -e "s#\${${n#PROMETHEUS_}}#${v}#g" /etc/prometheus/prometheus.yml.tmpl
    fi
done

generate_prometheus_config

reload_prometheus_config &

/bin/prometheus "$@"
